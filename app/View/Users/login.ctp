<form action="#" class="form-signin" method="post" action="">
    <h2 class="form-signin-heading">Please sign in</h2>
    <!-- Username -->
    <label class="sr-only" for="inputUsername">Username</label>
    <input type="text" autofocus="" required placeholder="Username" class="form-control" id="inputUsername"
           name="data[TblUsers][username]">
    <!-- Password -->
    <br/>
    <label class="sr-only" for="inputPassword">Password</label>
    <input type="password" required placeholder="Password" class="form-control" id="inputPassword"
           name="data[TblUsers][password]">
    <br/>
    <!-- Submit btn -->
    <p><a href="register.php">Chưa có tài khoản, đăng kí ở đây</a></p>
    <br/>
    <button type="submit" name="signin" class="btn btn-lg btn-primary btn-block">Sign in</button>
</form>