<?php
App::uses('AppController', 'Controller');

/**
 * Users Controller
 */
class UsersController extends AppController
{

    var $uses = array('TblUsers');
    /**
     * Scaffold
     *
     * @var mixed
     */

    public function beforeFilter()
    {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('add', 'logout');
    }

    public function login()
    {
        if ($this->request->is('post')) {
            var_dump($this->request->data);
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
        $this->render('login','login');
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
}
